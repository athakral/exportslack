var _ = require('lodash');
var rest = require('restler');
var jsonfile = require('jsonfile');
var util = require('util');
var config = require('./config.json');
var moment = require('moment');


var tokenParam = '?token=' + config.token;
var currentUser = {};

var generateFile = function(messages, otherUser) {
  var outputFile = __dirname + config.outputDir + config.groupName + "_" + currentUser.name + "_" + otherUser.name + ".json";
 var convertedMessages =_.map(_.sortBy(messages,"ts"),function (value){
   value.userName = value.user === currentUser.id ? currentUser.name : otherUser.name;
   value.timeStamp = moment.unix(value.ts).format("MM/DD/YYYY HH:mm:ss");
   delete value.ts;
   delete value.user;
   delete value.type;
   return value;
 });
  jsonfile.writeFile(outputFile, convertedMessages, {
    spaces: 2
  }, function(err) {
    if (err)
      console.log("Error Writing file :: " + outputFile)

    console.log("generated :: "+outputFile);
  });
};

// Check if token is valid, if it is use that info as a current user
rest.get(config.baseUrl + config.methods.auth + tokenParam).on('complete', function(data, response) {
  if (data instanceof Error) {
    console.log('Error:', data.message);
    this.retry(5000); // try again after 5 sec
  } else {
    if (!data.ok) {
      console.log("Invalid Auth Token, please make sure the token is correct");
      process.exit(0);
    }
    currentUser.id = data.user_id;
    currentUser.name = data.user;
    currentUser.real_name = "";

    console.log("Got the current user");
    //console.log(currentUser);

    //get the list of all users -- used to generate the file later
    rest.get(config.baseUrl + config.methods.users + tokenParam).on('complete', function(data, response) {
      if (data instanceof Error) {
        console.log('Error:', data.message);
        this.retry(5000); // try again after 5 sec
      } else {
        if (!data.ok) {
          console.log("Something is wrong, while retriving the list of users");
          process.exit(0);
        }
        var userList = _.map(data.members, function(value) {
          return {
            id: value.id,
            name: value.name,
            real_name: value.name
          }
        });
        userList.push(currentUser);
        //console.log(userList);

        //get list of all Dms
        rest.get(config.baseUrl + config.methods.allDMs + tokenParam).on('complete', function(data, response) {
          if (data instanceof Error) {
            console.log('Error:', data.message);
            this.retry(5000); // try again after 5 sec
          } else {
            if (!data.ok) {
              console.log("Something is wrong, while retriving the list of all dms");
              process.exit(0);
            }
            var allDms = _.map(data.ims, function(value) {
              return {
                id: value.id,
                userid: value.user,
              }
            });



            //for each DMs get the messages
            _.each(allDms, function(dm) {

              var messageOtherUser = _.find(userList, function(u) {
                return u.id === dm.userid;
              });

              //console.log(dm.userid);

              if (!messageOtherUser)
                return;

              //console.log(messageOtherUser);


              var getAllMessages = function(oldMessages, latestvalue, finalCallback) {
                oldMessages = oldMessages || [];
                var latesValueParam = latestvalue ? "&latest=" + latestvalue : "";
                //console.log(config.baseUrl + config.methods.messages + tokenParam + "&count=1000&channel=" + dm.id  + latesValueParam);
                rest.get(config.baseUrl + config.methods.messages + tokenParam + "&count=1000&channel=" + dm.id  + latesValueParam)
                  .on('complete', function(data, response) {
                    if (data instanceof Error) {
                      console.log('Error:', data.message);
                      this.retry(5000); // try again after 5 sec
                    } else {
                      if (!data.ok) {
                        console.log("Something is wrong, while retriving messages");
                        process.exit(0);
                      }

                      //console.log(data.messages);
                      if (data.messages.length === 0)
                        return;

                      Array.prototype.push.apply(oldMessages, data.messages);

                      if (data.has_more) {
                        //console.log("has more messages");
                        //console.log(messageOtherUser.name);
                        var nextLatestValue = _.last(data.messages).ts;
                        getAllMessages(oldMessages, nextLatestValue, finalCallback);
                      } else {
                        finalCallback(oldMessages);
                      }

                    }
                  });
              };

              getAllMessages(null, null, function(allMessages) {
                generateFile(allMessages, messageOtherUser);
              });

            });




          }
        });


      }
    });

  }
});
